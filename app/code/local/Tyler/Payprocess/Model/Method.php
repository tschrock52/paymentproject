<?php
 
/**
* Module Adapter. Extends the Mage_Payment_Model_Method_Cc with class Tyler_Payprocess_Model_Method.
*/
class Tyler_Payprocess_Model_Method extends Mage_Payment_Model_Method_Cc
{

    protected $_code = 'payprocess';

    /**
     * Is the payment method a Gateway?
     */
    protected $_isGateway               = true;
 
    /**
     * Can this payment method authorize online?
     */
    protected $_canAuthorize            = true;
 
    /**
     * Can this payment method capture funds online?
     */
    protected $_canCapture              = true;
 
    /**
     * Can this payment method capture partial amounts online?
     */
    protected $_canCapturePartial       = false;
 
    /**
     * Can this payment method refund online?
     */
    protected $_canRefund               = false;
 
    /**
     * Can this payment method void transactions online?
     */
    protected $_canVoid                 = true;
 
    /**
     * Can you use this payment method in administration panel?
     */
    protected $_canUseInternal          = true;
 
    /**
     * Will you show this payment method as an option on checkout payment page?
     */
    protected $_canUseCheckout          = true;
 
    /**
     * Is this payment method suitable for multi-shipping checkout?
     */
    protected $_canUseForMultishipping  = true;
 
    /**
     * Can save credit card information for future processing?
     */
    protected $_canSaveCc = false;
 
    /**
     * Implementing public, capture, and void public functions
     */
}
?>